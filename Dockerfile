FROM ubuntu:22.04
LABEL maintainer="Justin Seliga <gitlab@emails.justin.seliga.me>"
ARG DEBIAN_FRONTEND=noninteractive

ADD build.sh /
RUN chmod +x /build.sh
RUN /build.sh
RUN rm -rf /build.sh
