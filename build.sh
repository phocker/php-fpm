#!/bin/bash
apt-get update -y
apt-get install -y software-properties-common

# PHP repository
add-apt-repository ppa:ondrej/php -y
apt-get update -y

# Install and configure PHP
apt-get install -y php5.6-fpm
update-alternatives --set php /usr/bin/php5.6

# Install dependencies
apt-get install -y --no-install-recommends \
    curl \
    libfreetype6-dev \
    libjpeg-dev \
    libmemcached-dev \
    libmcrypt-dev \
    libonig-dev \
    libpng-dev \
    libpq-dev \
    libssl-dev \
    libwebp-dev \
    libxpm-dev \
    libz-dev

# Install extensions
apt-get install -y \
    php5.6-bz2 \
    php5.6-bcmath \
    php5.6-curl \
    php5.6-gd \
    php5.6-imagick \
    php5.6-imap \
    php5.6-intl \
    php5.6-json \
    php5.6-ldap \
    php5.6-libvirt-php \
    php5.6-lua \
    php5.6-mbstring \
    php5.6-mcrypt \
    php5.6-mongodb \
    php5.6-mysql \
    php5.6-memcached \
    php5.6-pgsql \
    php5.6-redis \
    php5.6-smbclient \
    php5.6-soap \
    php5.6-sqlite3 \
    php5.6-ssh2 \
    php5.6-xml \
    php5.6-yaml \
    php5.6-zip

# Clean up
apt-get autoclean
apt-get autoremove
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
rm -rf /var/tmp/*
